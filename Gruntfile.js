module.exports = function(grunt) {
  grunt.initConfig({
    sass: {
      options: {
        sourcemap: true
      },
      dist: {
        files: {
          'pub/css/build.css': 'dev/scss/style.scss'
        }
      }
    },
    autoprefixer: {
        dist: {    
            files: {
                'pub/css/style.css': 'pub/css/build.css'
            }
        }
    },
    includereplace: {
        your_target: {
          options: {
              prefix:'<!--@@',
              suffix:'-->'
          },
          files:[
            { src: 'dev/html/index.html', dest: 'index.html' },
            { src: 'dev/html/signin.html', dest: 'signin.html' }
          ]
        }
    },
    connect: {
      server: {
        options: {
          protocol: 'http',
          port: 8000,
          keepalive: false,
          base: '.'
        }
      }
    },
    watch: {
      source: {
        files: ['dev/**/*.scss',
                'dev/html/**/**/*.html',
                'pub/js/*.js'],
        tasks: ['sass','includereplace', 'autoprefixer'],
        options: {
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-include-replace');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.registerTask('default', ['sass', 'autoprefixer', 'connect', 'watch' ]);
};
