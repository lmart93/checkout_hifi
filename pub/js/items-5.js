// var items = Cookies.getJSON();

if( $.isEmptyObject(items) ){

var items = [
        {
            name: 'KitchenAid® Artisan® 5-qt. Stand Mixer KSM150P',
            img: 'product1',
            brand: 'Kitched Aid',
            stock: 20,
            options:{
                edit:false,
                color:{
                    set:'Apple Red',
                    colors:[
                        {
                            name:'White',
                            value:'#fff'
                        },
                        {
                            name:'Fusia',
                            value:'#ff66cc'
                        },
                        {
                            name:'Orange',
                            value:'#ff7f50'
                        },
                        {
                            name:'Apple Read',
                            value:'#cc0000'
                        },
                        {
                            name:'Coral',
                            value:'#DC143C'
                        },
                        {
                            name:'Dark Khaki',
                            value:'#BDB76B'
                        },
                        {
                            name:'Cyan',
                            value:'#00ffff'
                        }
                    ]
                }
            },
            price:{
                fprice: 349.99,
                original:500.00,
                sale: 349.99,
                coupon: {
                    amt: 0
                },
                reward: {
                    amt: 0,
                    show:false
                },
                show:false
            },
            fulfillment:'shiponly',
            bopis:false,
            ship:true,
            shipto:'',
            estDelivery:'July 12th - July 16th',
            messaging:{
                oversize:9.85
            },
            remove:false
        },
        {
            name: 'St. John\'s Bay® Short-Sleeve V-Neck T-Shirt',
            img: 'product2',
            stock: 2,
            brand: 'St. John\'s',
            options:{
                edit:false,
                size:{
                    set:'Medium',
                    sizes:[
                           'Extra Small',
                           'Small',
                           'Medium',
                           'Large'
                        ]
                },
                color:{
                    set:'Orange',
                    colors:[
                        {
                            name:'Orange',
                            value:'#ff7f50'
                        },
                        {
                            name:'Fusia',
                            value:'#ff66cc'
                        },
                        {
                            name:'Coral',
                            value:'#DC143C'
                        },
                        {
                            name:'Dark Khaki',
                            value:'#BDB76B'
                        },
                        {
                            name:'Cyan',
                            value:'#00ffff'
                        }


                    ]
                }
            },
            price:{
                fprice: 9.80,
                regular:9.80,
                coupon: {
                    amt: 0
                },
                reward: {
                    amt: 3,
                    show:false
                },
                show:false
            },
            fulfillment:'bopis',
            bopis:false,
            ship:true,
            shipto:'',
            estDelivery:'July 12th - July 16th',
            remove:false
        },
        {
            name: 'Serta® Perfect Sleeper® Hiddenvale Euro Top - Mattress Only',
            img: 'product3',
            stock: 20,
            brand: 'Serta® Perfect Sleeper®',
            options:{
                edit:false,
                size:{
                    set:'King',
                    edit:false,
                    sizes:[
                           'Queen',
                           'Full',
                           'Twin',
                        ]
                },
                color:{
                    set:'Pearl White',
                    edit:false,
                    colors:[
                        {
                            name:'Pearl White',
                            value:'#fafafa'
                        },
                        {
                            name:'Gray',
                            value:'#ccc'
                        },
                    ]
                }
            },
            price:{
                fprice: 250.00,
                regular:250.00,
                coupon: {
                    amt: 0
                },
                reward: {
                    amt: 5,
                    show:false
                },
                show:false
            },
            fulfillment:'truck',
            bopis:false,
            ship:true,
            shipto:'',
            estDelivery:'July 12th',
            messaging:{
                recycle:true
            },
            remove:false
        },
        {
            name: 'Call It Spring™ Sevuviel Mens Oxford Dress Shoes',
            img: 'product5',
            stock: 20,
            brand: 'Call It Spring™',
            options:{
                edit:false,
                color:{
                    set:'Black',
                    colors:[
                        {
                            name:'Black',
                            value:'#333'
                        },
                        {
                            name:'Saddle Brown',
                            value:'#8B4513'
                        },
                    ]
                },
                size:{
                    set:'9',
                    edit:false,
                    sizes:[
                           '8',
                           '8.5',
                           '9',
                           '9.5',
                           '10',
                           '11'
                        ]
                },
                width: {
                    set:'wide',
                    edit:false,
                    sizes:[
                           'medium',
                           'large',
                        ]
                },

            },
            price:{
                fprice: 60.00,
                original:90.00,
                sale: 60.00,
                coupon: {
                    amt: 0
                },
                show:false
            },
            fulfillment:'bopis',
            bopis:true,
            ship:false,
            shipto:'',
            estDelivery:'July 12th - July 16th',
            remove:false
        },
        {
            name: 'i jeans by Buffalo Sleeveless Illusion Midi Dress',
            img: 'product6',
            stock: 20,
            brand:'i jeans',
            options:{
                edit:false,
                color:{
                    set:'Blue',
                    colors:[
                        {
                            name:'Black',
                            value:'#333'
                        },
                        {
                            name:'Saddle Brown',
                            value:'#8B4513'
                        },
                    ]
                },
                size:{
                    set:'Small',
                    edit:false,
                    sizes:[
                           '8',
                           '8.5',
                           '9',
                           '9.5',
                           '10',
                           '11'
                        ]
                }
            },
            price:{
                fprice: 44.00,
                original:44.00,
                coupon: {
                    amt: 0
                },
                show:false
            },
            fulfillment:'bopis',
            bopis:true,
            ship:false,
            shipto:'',
            estDelivery:'July 12th - July 16th',
            remove:false
        }
    ];
    console.log('item obj created',items);
}else{
    var tmp = [];
    var i=0;
    for(var prop in items){
        if(items.hasOwnProperty(prop)){
            tmp[i] = items[prop];
            i++;
        }
    }
    var items = tmp;
    console.log('cookie exists',items);
}
