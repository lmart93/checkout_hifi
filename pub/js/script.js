
var app = angular.module('COProto', []);
app.controller('CO', function($scope, $timeout) {

    $scope.items = items;

    //sticky functionally
    $(function () {
      var distancePayment = $('#bottom').offset().top,
          $window = $(window);

      $window.scroll(function(){
        if ( $window.scrollTop() >= distancePayment ) {
          $('#bottom').addClass('sticky-pricing');
          $('#steps-holder').addClass('sticky-steps');
        } else {
          $('#bottom').removeClass('sticky-pricing');
          $('#steps-holder').removeClass('sticky-steps');
        }
      });
    });

    //set progress step
    $scope.progress = 1;

    //set steps obj
    $scope.steps = {
        sameday: true
    }
    $scope.loading = false;
    $scope.nextStep = 'CONTINUE TO SHIPPING';

    /*
    $scope.returnSteps = function(step){
      if ($scope.progress > step) {
        for(var prop in $scope.steps){
          if($scope.steps.hasOwnProperty(prop)){
            $scope.steps[prop] = false;
          }
        }
        console.log($scope.steps);

        switch(step){
          case 1:
            $scope.steps.sameday = true;
          break;
          case 2:
            $scope.steps.shipping = true;
          break;
          case 3:
            $scope.steps.payment = true;
          break;
          case 4:
            $scope.steps.review = true;
          break;
        }
        console.log($scope.steps);
      }
    }
    */

    // function to validate shipping and payment form
    function validate(){
      if($scope.shippingForm.fname){
        return true;
        alert('yes');
      } else {
        alert('no');
        return false;
      }
    }

    function showLoading(){
        $scope.loading = true;
        $timeout(function(){ $scope.loading = false }, 2000);
    }

    $scope.paymentForm = {};

    //continue function
    $scope.continue = function(){

        // true == current step, false == done with step, null == yet to come
        switch($scope.progress){
            //at bopis
            case 1:
                showLoading();
                $scope.steps.sameday = false;
                $scope.steps.shipping = true;
                $scope.nextStep = 'CONTINUE TO PAYMENT';
                $scope.progress++;

            break;
            //at shipping
            case 2:

                // $scope.progress++;
                showLoading();

                if ($scope.shippingForm.fname && $scope.shippingForm.lname && $scope.shippingForm.field) {
                  $scope.nextStep = 'CONTINUE TO REVIEW';
                  $scope.progress++;
                  $scope.steps.shipping = false;
                  $scope.steps.payment = true;
                } else {
                  // init errors
                  $scope.shippingFormError = true;
                  $scope.lnameError = true;
                  $scope.phoneError = true;
                  $scope.streetError = true;

                  // first name
                  $scope.$watch('shippingForm.fname', function(newValue){
                    if(newValue.length === 0){
                      $scope.shippingFormError = true;
                      $scope.shippingFormValid = false;
                    } else {
                      $scope.shippingFormError = false;
                      $scope.shippingFormValid = true;
                    }
                  });

                  // last name
                  $scope.$watch('shippingForm.lname', function(newValue){
                    if(newValue.length === 0){
                      $scope.lnameError = true;
                      $scope.lnameValid = false;
                    } else {
                      $scope.lnameError = false;
                      $scope.lnameValid = true;
                    }
                  });

                  // phone
                  $scope.$watch('shippingForm.phone', function(newValue){
                    if(newValue.length === 0){
                      $scope.phoneError = true;
                      $scope.phoneValid = false;
                    } else {
                      $scope.phoneError = false;
                      $scope.phoneValid = true;
                    }
                  });

                  // street address
                  $scope.$watch('shippingForm.field', function(newValue){
                    if(newValue.length === 0){
                      $scope.streetError = true;
                      $scope.streetValid = false;
                    } else {
                      $scope.streetError = false;
                      $scope.streetValid = true;
                    }
                  });
                }

            break;
            //at payment
            case 3:

                showLoading();
                if ($scope.shippingForm.phone && $scope.paymentForm.email) {
                  $scope.progress++;
                  $scope.steps.payment = false;
                  $scope.steps.review = true;
                  $('#top').slideDown(1000);
                  $('#checkout-cta').fadeIn(1000);
                  $('html,body').animate({ scrollTop: 0 }, 500);
                } else {

                  // init errors
                  if ($scope.paymentForm.phone == '') {
                    $scope.paymentPhoneError = true;
                  } else {
                    $scope.paymentForm.Error = false;
                  }
                  $scope.paymentEmailError = true;

                  // phone
                  $scope.$watch('shippingForm.phone', function(newValue){
                    if(newValue.length === 0){
                      $scope.paymentPhoneError = true;
                      $scope.paymentPhoneValid = false;
                    } else {
                      $scope.paymentPhoneError = false;
                      $scope.paymentPhoneValid = true;
                    }
                    console.log('hello',$scope.shippingForm.phone);
                  });

                  // email
                  $scope.$watch('paymentForm.email', function(newValue){
                    if(newValue.length === 0){
                      $scope.paymentEmailError = true;
                      $scope.paymentEmailValid = false;
                    } else {
                      $scope.paymentEmailError = false;
                      $scope.paymentEmailValid = true;
                    }
                  });
                }

            break;
            case 4:
                showLoading();
                $scope.steps.review = false;
                $scope.steps.confirmation = true;
                $scope.progress++;
                $('#top').slideUp();
                $('#checkout-cta').fadeOut();
                $('#steps-holder').addClass('done');
                $('#bottom').addClass('lock');
            break;
            console.log($scope.progress);

        }
    }

    $scope.gotoStep = function(n){
        console.log('step',n);

        switch(n){
            case 2:
               $scope.steps.sameday = true;
               delete $scope.steps.shipping;
               $scope.nextStep = 'CONTINUE TO SHIPPING';
               $scope.progress--;
            break;
            case 3:
               $scope.steps.shipping = true;
               delete $scope.steps.payment;
               $scope.nextStep = 'CONTINUE TO PAYMENT';
               $scope.progress--;
            break;
            case 4:
               $scope.steps.payment = true;
               delete $scope.steps.review;
               $scope.nextStep = 'CONTINUE TO REVIEW';
               $scope.progress--;
            break;
        }
    }

    // calendar widget
    $scope.calendar = false;
    $scope.date = ['May','10th, Wednesday'];

    //init
    $scope.openCalendar = function(n){
        $scope.calendar = true;
    }

    // when user applies rewards number
    $scope.rewardsForm = "";
    $scope.submitRewards = function(){
      if ($scope.rewardsForm === '8797') {
        $scope.finalTotal -=  10;
        $('#totaldiscounts').slideDown(200);

        //reset input field
        $scope.rewardSuccess = true;
        $scope.rewardsInput = false;
        $scope.code = '';
        $scope.couponInvalid = false;

        //set new item price & total savings
        items[1].price.fprice -= 10;
        $scope.totalSavings += 10;

      }
    }

    // coupon and rewards
    $scope.appliedrewards = false;
    $scope.appliedcoupons = false;
    $scope.serialEntry = false;
    $scope.closeCR = false;

    //coupons set array
    $scope.coupons = [];
    var coupons = $scope.coupons;

    //rewards set array
    $scope.rewards = [];
    var rewards = $scope.rewards;

    $scope.crErrorMsgs = [
            'we did not recognize this code',
            'this coupon is already applied'
        ];

    $scope.crErrorMsg = '';

    $scope.codeEntry = function(n){
        $scope.entryError = false;

        if(n!=undefined){
            n = n.toLowerCase();

            switch(n){
                case'fundeal':

                    var coupon = {
                        name: 'FUNDEAL',
                        discount: 2.4,
                        items: [0,1]

                    }

                    $scope.entryError = false;
                    $scope.success = false;
                    successEntry();

                break;
                case 'jcpsale2':

                    var coupon = {
                        name: 'JCPSALE2',
                        discount: 3.4,
                        items: [3,4]
                    }

                    $scope.entryError = false;

                break;
                case '123456':

                    $scope.entryError = false;
                    $scope.serialEntry = true;

                break;
                default:
                    $scope.entryError = true;
            }

            function successEntry(){

                    coupons.push(coupon);
                    priceCalculate(coupon.items,coupon.discount);

                    $scope.code = '';
                    $scope.appliedcoupons = true;
                    $scope.closeCR = true;
                    console.log(coupon.items);

            }

        }else{
            $scope.entryError = true;
            $scope.crErrorMsg = $scope.crErrorMsgs[0]
        }
    }

    //price calculate after coupon
    function priceCalculate (applieditems,discount) {
        for(var propt in items){
            var x = parseInt(propt);
            var c = applieditems.indexOf(x);
            if(c != -1){
                items[propt].price.coupon.show=true;
                items[propt].price.coupon.amt=-(discount/applieditems.length);
                items[propt].price.fprice-=(discount/applieditems.length);
            }else{
                items[propt].price.coupon.show=false;
            }
        }
        $scope.totalCoupon = discount;
        $scope.finalTotal -=  $scope.totalCoupon;
        $scope.totalSavings = 120.1+2.4;
    }

    // Calculate Sub Total
    var total = 0;
    for(count=0; count<$scope.items.length; count++){
        total += $scope.items[count].price.fprice;

        if($scope.items[count].price.coupon.show != undefined){
            var addCoupon = true;
        }
    }
    //console.log($scope.items[count].price.coupon.show);
    $scope.subTotal = total;
    $scope.finalTotal = $scope.subTotal;

    $scope.extraCharges = [34,28,00];

    if(addCoupon == true){
        $scope.totalSavings = 120.1+2.4;
    }else{
        $scope.totalSavings = 120.1;
    }


    $scope.shippingFormHide = true;

    // set address - shipping panel
    $scope.setAddress = function(address) {
      this.shippingForm.field = address;
      $scope.showForm = true;
      $scope.hideForm = true;

      $scope.shippingFormHide = true;
      $('#form').hide();

      $scope.showShippingMethod = true;
    }

    // payment tabs
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };


    // shipping methods
    //go to checkout
    $scope.goToCheckout = function(){
        window.location = 'index.html';
    }

    // shipping panels
    $scope.smTab = 1;

    $scope.shipToHomeValue === 'FREE';

    $scope.setSMTab = function(newTab, value){
      $scope.smTab = newTab;

      // add shipping method price to final total
      $scope.finalTotal += value;
      $scope.shipToHomeValue = value;
    };

    $scope.isSMSet = function(tabNum){
      return $scope.smTab === tabNum;
    };

    // grab contact info to be used in review section
    $scope.shippingForm = {};

    //bopis form
    $scope.iwill = function(){
        $('#someoneelse').slideToggle();
    }
    $scope.someoneelse = function(){
        $('#someoneelse').slideToggle();
    }

    //sts person
    $scope.iwillSTS = function(){
        $('#someoneelseForm').slideToggle();
    }
    $scope.someoneelseSTS = function(){
        $('#someoneelseForm').slideToggle();
    }

    //toggle billing form in payment
    $scope.billingToggle = function(){
        $("#billingForm").slideToggle();
        $("#billingSummary").slideToggle();
    }

    //global payment switch
    $scope.paymentViewToggle = function(){
        $("#paymentGuest").toggle();
        $("#paymentRegistered").toggle();
    }

    //global shipping switch
    $scope.shippingViewToggle = function(){
        $("#shippingGuest").toggle();
        $("#shippingRegistered").toggle();
    }

    //add new card
    $scope.addNewCard = function(){
        $('#newCardEntry').slideDown();
        $('#newCard').addClass('active active-new');
    }

    //add new addres
    $scope.addNewAddress = function(){
        $('#addNewAddress').addClass('active active-new');
        $('#newAddressForm').slideDown();
    }

    //sms opt-in
    $scope.smsOptIn = function(){
        $("#smsPhone").fadeToggle();
    }

    $scope.selectCard = function(n,nc){
        console.log(angular.element(n.currentTarget));

        $('.card').removeClass('active active-new');
        angular.element(n.currentTarget).addClass('active');

        $('#newCardEntry').slideUp();
        $('#newCard').removeClass('active active-new');

        if(nc){
            $('#newCardEntry').slideDown();
            $('#newCard').addClass('active active-new');
        }
    }

});
